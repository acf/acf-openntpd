local mymodule = {}

mymodule.default_action = "status"

function mymodule.config(self)
	return self.handle_form(self, self.model.read_config, self.model.update_config, self.clientdata, "Save", "Edit OpenNTPD Config", "OpenNTPD Configuration Saved")
end

function mymodule.status (self)
	return self.model.getstatus()
end

function mymodule.details (self)
	return self.model.getstatusdetails()
end

function mymodule.expert (self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
